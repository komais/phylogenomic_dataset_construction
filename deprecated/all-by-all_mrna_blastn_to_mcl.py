"""
Main difference between mrna vs cds or peptides is that mrna has chimeras from both direction
Do not expand hit fraction. Only take the top hits
"""

import os,sys
import math

def get_minus_log_evalue(raw_value):
	try:    evalue = -math.log10(float(raw_value))
	except: evalue = 180.0 #largest -log10(evalue) seen
	return evalue

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python all-by-all_blast_to_mcl.py rawblast coverage_cut"
		sys.exit()
		
	COVERAGE_CUT = sys.argv[2]
	#both percent query and hit coverage have to >= this
	
	print "Reading raw blast output"
	infile = open(sys.argv[1],"r")
	outfile = open(sys.argv[1]+".coverage"+COVERAGE_CUT+".log-evalue","w")
	count = 0
	last_query,last_hit = "",""
	for line in infile:
		if len(line) < 3: continue #skip empty lines
		spls = line.strip().split("\t")
		query,hit = spls[0],spls[2]
		if query == hit: continue #ignore empty lines and self-hits
		qlen,qstart,qend = float(spls[1]),float(spls[10]),float(spls[11])
		slen,sstart,send = float(spls[3]),float(spls[12]),float(spls[13])
		evalue = get_minus_log_evalue(spls[14])
		if last_query == "": #at the very beginning of the blastn output
			max_evalue = evalue #record the highest evalue for each query-hit pair
		else: #not at the very beginning of the blastn output
			if query == last_query and hit == last_hit: pass #only take the top hit
			else:#summarize last query-hit pair
				perc_qrange = (last_qend - last_qstart + 1) / last_qlen
				perc_srange = (last_send - last_sstart + 1) / last_slen
				if perc_qrange >= float(COVERAGE_CUT) and perc_srange >= float(COVERAGE_CUT):
					#output info for the previous query-hit pair
					outfile.write(last_query+"\t"+last_hit+"\t"+str(max_evalue)+"\n")
					count += 1
					if count % 100000 == 0:
						print str(count)+" hits written to the output"
				max_evalue = evalue #reset max_evalue for a new query-hit pair
		last_query,last_qlen,last_qstart,last_qend = query,qlen,qstart,qend
		last_hit,last_slen,last_sstart,last_send = hit,slen,sstart,send
	infile.close()
	outfile.close()
			

