import sys,os

"""
use trimAl to trime alignment
then remove seqs that are very short or empty
"""
MIN_CHAR = 20

if len(sys.argv) != 2:
	print "python trimal_wrapper.py DIR"
	sys.exit(0)
	
DIR = sys.argv[1]+"/"
check = [] #store very skinny matrixes to check later
for i in os.listdir(DIR):
	if i[-3:] != "aln": continue
	com = "trimal -in "+DIR+i+" -out "+DIR+i+"-trim -automated1"
	print com
	os.system(com)
		
	seqid = ""
	first = True
	infile = open(DIR+i+"-trim","r")
	outfile = open(DIR+i+"-cln","w")
	for line in infile:
		line = line.strip()
		if len(line) == 0: continue #skip empty lines
		if line[0] == ">":
			if seqid != "": #not at the first seqid
				if first == True:
					length = len(seq)
					if length < 40: check.append(i+" "+str(length))
					#min_chr = max(float(occupancy)*length,40)
					first = False
					#print "length",length,"min_chr",min_chr
				if len(seq.replace("-","")) >= MIN_CHAR:
					outfile.write(">"+seqid+"\n"+seq+"\n")
			seqid,seq = line.split(" ")[0][1:],""
		else: seq += line.strip()
	#process the last seq
	if len(seq.replace("-","")) >= MIN_CHAR:
		outfile.write(">"+seqid+"\n"+seq+"\n")
	infile.close()
	outfile.close()
	
print "check these matrixes"
for j in check: print j
