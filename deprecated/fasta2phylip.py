"""
"@" in all ingroup seq ids, but not outgroups seq ids
convert cleaned alignment files end with cln to relaxed phylip format
"""

import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python fasta2phylip.py inDIR outDIR"
		sys.exit(0)
		
	inDIR = sys.argv[1]+"/"
	outDIR = sys.argv[2]+"/"
	for fasta_file in os.listdir(inDIR):
		if fasta_file[-3:] == "cln":
			#do not use biopython so that seq ids can be > 10 charactors
			longest_id = 1 #hate that phylip requires this! Why not using fasta!
			seqDICT = {} #key is seq id, value is seq
			infile = open(inDIR+fasta_file,"rU")
			seqid,seq = "",""
			for line in infile:
				line = line.strip()
				if len(line) > 1:
					if line[0] == ">":
						if seqid != "": seqDICT[seqid] = seq
						seqid,seq = line[1:],""
						longest_id = max(longest_id,len(seqid))
					else: seq += line.strip()
			if seq != "": seqDICT[seqid] = seq #add the last record
			infile.close()

			#output relaxed phylip file
			if len(seqDICT) > 0:
				phy_file = fasta_file+".phy"
				outfile = open(outDIR+phy_file, "w")
				aligned_length = len(seqDICT[seqDICT.keys()[0]])
				outfile.write(str(len(seqDICT))+" "+str(aligned_length)+"\n")
				for i in seqDICT: #loop through seq records
					outfile.write(i)
					for j in range(longest_id-len(i)+1): outfile.write(" ")
					outfile.write(seqDICT[i]+"\n")
				outfile.close()
