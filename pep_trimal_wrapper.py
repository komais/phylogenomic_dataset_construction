import sys,os
from Bio import SeqIO

"""
use trimAl to trime alignment
then remove seqs that are very short or empty
"""

MIN_SEQ_LEN = 10 #remove seq shorter than this
MIN_MATRIX_LEN = 50 #check matrix shorter than this

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python trimal_wrapper.py DIR"
		sys.exit(0)
		
	DIR = sys.argv[1]+"/"
	check = [] #store very skinny matrixes to check later
	for i in os.listdir(DIR):
		if i[-3:] != "aln": continue
		#com = "trimal -in "+DIR+i+" -out "+DIR+i+"-trim"+" -gt "+occupancy+" -st 0.0002"
		com = "trimal -in "+DIR+i+" -out "+DIR+i+"-trim"+" -automated1"
		print com
		os.system(com)
		
		#remove very short seq after trimming
		handle = open(DIR+i+"-trim","rU")
		outfile = open(DIR+i+"-cln","w")
		first = True
		for seq_record in SeqIO.parse(handle,"fasta"):
			seqid,seq = str(seq_record.id),str(seq_record.seq)
			if first:
				matrix_len = len(seq)
				if matrix_len < MIN_MATRIX_LEN:
					check.append(i)
				first = False
			seq_len = len(seq.replace("-",""))
			if seq_len >= MIN_SEQ_LEN:
				outfile.write(">"+seqid+"\n"+seq+"\n")
			else: print seqid,"shorter than",MIN_SEQ_LEN
		handle.close()
		outfile.close()
		
	print "check these matrixes"
	for j in check: print j
